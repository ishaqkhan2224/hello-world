<footer id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <a href="#" class="footer-logo">
                    <img class="footer-logo" src="./assets/images/assets/footer/logo-qe.png" alt="Logo">
                </a>
                <ul class="footer-social-links mt-2">
                    <li>
                        <a href="https://play.google.com/store/apps/details?id=com.qatarevents" title="" target="_blank" class="footer-download__item">
                            <img src="./assets/images/assets/footer/playmarket.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="https://itunes.apple.com/us/app/qatar-events-app/id1350228281?ls=1&amp;mt=8" title="" target="_blank" class="footer-download__item">
                            <img src="./assets/images/assets/footer/appstore.png" alt="">
                        </a>
                    </li>
                    <li><a href="https://www.facebook.com/QatarEvents/" title="" target="_blank" class="footer-social__item _fb"></a></li>
                    <li><a href="https://www.instagram.com/qatarevents/" title="" target="_blank" class="footer-social__item _in"></a></li>
                    <li><a href="https://twitter.com/qatarevents" title="" target="_blank" class="footer-social__item _tw"></a></li>
                </ul>
            </div>
            <div class="col-md-9">
                <ul class="footer-menu">
                    <li><a href="#about">About Us</a></li>
                    <li><a href="#features">Features</a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="#">Advertising</a></li>
                    <li><a href="#">Feedback</a></li>
                </ul>
                <div class="row align-items-end">
                    <div class="col-md-8">
                        <div class="powered-by">
                            <span>Powered by</span>
                            <a href="#"><img class="img-fluid" src="./assets/images/assets/footer/iloveq-logo.png" alt=""></a>
                        </div>
                        <div class="footer-copyright">All Right Reserved Qatar Events2019</div>
                    </div>
                    <div class="col-md-4">
                        <div class="footer-signup-title">Sign up for our newsletter</div>
                        <form class="footer-signup-form" target="_blank" novalidate method="post" action="//iloveqatar.us1.list-manage.com/subscribe/post?u=e9f0f67db228cda0f89addf6e&id=c994ef2dc2">
                            <input type="text" value="" name="EMAIL">
                            <button type="submit">Yalla!</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="modal fade qe-modal login-modal" id="login_form_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <h3 class="modal-title"><img class="img-fluid mr-2" src="./assets/images/logo-small.png" alt="">Login</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="modal-form login-form">
                    <form method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="firstName" class="form-control" placeholder="UserName" value="">
                                    <i class="fas fa-user"></i>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Password" value="">
                                    <i class="fas fa-lock"></i>
                                </div>
                            </div>
                            <div class="text-center w-100">
                                <button type="submit" class="btn btn-submit mb-4">Yalla! <i class="ml-5 fas fa-check"></i></button>
                                <a href="#" class="form-link d-block">Forget Password!</a>
                            </div>
                            <div class="follow-us-links">
                                <span>Or signup with:</span>
                                <a href="#" class="face-book"><i class="fab fa-facebook-f"></i> Facebook</a>
                                <a href="#" class="twitter"><i class="fab fa-twitter"></i> Twitter</a>
                            </div>
                            <h5 class="text-center mt-4 w-100">Don't have an account? <a href="#" class="toggle-register-modal">Register</a></h5>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade qe-modal register-modal" id="register_form_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><img class="img-fluid mr-2" src="./assets/images/logo-small.png" alt="">Register</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="modal-form register-form">
                    <form method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Username" value="">
                                    <i class="fas fa-user"></i>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Password" value="">
                                    <i class="fas fa-lock"></i>
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" placeholder="Mobile Number (optional)" value="">
                                    <i class="fas fa-mobile-alt"></i>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email Address" value="">
                                    <i class="fas fa-envelope"></i>
                                </div>

                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Confirm Password" value="">
                                    <i class="fas fa-check"></i>
                                </div>

                                <div class="form-group">
                                    <select class="form-control">
                                        <option selected>Choose Time Zone</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="agree-text">By pressing the "Submit" button you are agreeing to our company's <a href="#" class="form-link">Privacy policy and Terms of use</a></p>
                            <button type="submit" class="btn btn-submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade qe-modal contact-us-modal" id="contact_us_form_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <img class="gradient-line" src="./assets/images/line-img.png" alt="">
                <h3 class="modal-title">Contact Us Today</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">

                <div class="modal-form contact-us-form">
                    <form method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="First Name" value="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Last Name" value="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Email Address" value="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Comment"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12 col-lg-6">
                                <input type="text" class="form-control" placeholder="Phone Number" value="">
                            </div>
                            <div class="form-group col-12 col-lg-6 m-0 text-center">
                                <img class="img-fluid" src="./assets/images/recaptcha-img.png" alt="">
                            </div>
                        </div>
                        <div class="text-center mt-5">
                            <button type="submit" class="btn btn-submit">Submit<i class="fas fa-check"></i></button>
                        </div>
                        <div class="follow-us-links">
                            <span>Follow Us on:</span>
                            <a href="#" class="face-book"><i class="fab fa-facebook-f"></i> Facebook</a>
                            <a href="#" class="twitter"><i class="fab fa-twitter"></i> Twitter</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="./assets/js/lib/jquery.2.1.4.min.js"></script>
<script src="./assets/js/lib/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="./assets/js/lib/owlCarousel/owl.carousel.min.js"></script>
<script src="./assets/js/lib/jquery-validate/jquery.validate.min.js"></script>
<script src="./assets/js/custom.js"></script>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBJAoziLYyiUqCkUo3Q3PxdMrJeFYZCX-U&amp;language=en&amp;libraries=places&amp;callback=initAutocomplete" async="" defer=""></script>
<script>
    function initAutocomplete() {
        var styledMapType = new google.maps.StyledMapType(
            [
                {
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        }
                    ]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#bdbdbd"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#eeeeee"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e5e5e5"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#dadada"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e5e5e5"
                        }
                    ]
                },
                {
                    "featureType": "transit.station",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#eeeeee"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#c9c9c9"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                }
            ],
            {name: 'Styled Map'});

        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            center: {lat: 25.286106, lng: 51.534817},
            zoom: 13,
            //- mapTypeId: 'roadmap',
            disableDefaultUI: true,
            types: ['(cities)'],
            componentRestrictions: {country: 'qa'},
            mapTypeControlOptions: {
                mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                    'styled_map']
            }
        });
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');

        // Create the search box and link it to the UI element.
        var input = document.getElementById('address');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        google.maps.event.addDomListener(input, 'keydown', function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
            }
        });

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();

            places.forEach(function(place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    //- url: 'http://image.flaticon.com/icons/svg/252/252025.svg',
                    url: 'https://www.iloveqatar.net/static/images/content/map-marker.svg',
                    scaledSize: new google.maps.Size(40, 40),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(32,65),
                    labelOrigin: new google.maps.Point(20,-13)
                };

                var markerLabel = place.name;

                var marker = new google.maps.Marker({
                    map: map,
                    position: place.geometry.location,
                    icon: icon,
                    label: {
                        text: markerLabel,
                        color: "#8C529A",
                        fontSize: "23px",
                        fontWeight: "bold"
                    }
                });

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }

                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();

                document.getElementById('latitude').value = latitude;
                document.getElementById('longitude').value = longitude;

            });
            map.fitBounds(bounds);
        });
    }
</script>

<script>
    jQuery(document).ready(function ($) {

        jQuery('.events-carousel').owlCarousel({
            loop: true,
            margin: 30,
            items: 1,
            nav: true,
            dots: false,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

        });
        flatpickr("#eventsDatePicker", {
            format: 'L',
            inline: true,
            sideBySide: true
        });

        flatpickr("#from_date", {});
        flatpickr("#to_date", {});

        $(".event-menu-items li a").click(function () {
            $(".event-menu-items li a").removeClass("active");
            $(this).addClass("active");
        });

        $(".latest-events li a").click(function () {
            $(".latest-events li a").removeClass("active");
            $(this).addClass("active");
        });

        flatpickr("#createEventFromDate", {});
        flatpickr("#createEventToDate", {});
        flatpickr("#createEventFromTime", {
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i"
        });
        flatpickr("#createEventToTime", {
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i"
        });

    });// End of document ready function


</script>

</body>

</html>