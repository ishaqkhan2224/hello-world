<?php include_once "header.php" ?>
    <main class="main bg-darkgray">
        <div class="container bg-light">
            <section class="section">
                <div class="row">
                    <div class="col-md-9">
                        <?php require "./inc/advertisement.php" ?>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-8 col-lg-9">
                        <h1 class="form-title">CREATE EVENT</h1>
                        <form id="createEventForm" class="create-event-form">
                            <div id="ajax_error" class="text-danger mb-4" style="display:none;">
                                <strong><span id="ajax_error_mess"></span></strong>
                            </div>
                            <div class="form-group">
                                <label>HEADLINE*</label>
                                <input id="head-line" type="text" class="form-control" placeholder="What's your event called?">
                                <small class="form-text text-muted">*Enter a title for this event</small>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label>CHOOSE SUB-CATEGORY*</label>
                                    <select id="entry_type" class="form-control">
                                        <option value="">Select</option>
                                        <option value="15">ENTERTAINMENT</option>
                                        <option value="11">SPORTS</option>
                                        <option value="40">FOOD &amp; DINING</option>
                                        <option value="13">ARTS &amp; CULTURE</option>
                                        <option value="16">NIGHT</option>
                                        <option value="14">COMMUNITY</option>
                                        <option value="10">OTHER</option>
                                        <option value="85">VOLUNTEER</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-7">
                                    <label>COVER IMAGE*</label>
                                    <div class="custom-file">
                                        <input id="cover-image" type="file" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    <small class="form-text text-muted">*Maximum 1000x600px resolution</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>SUMMARY*</label>
                                <textarea id="summary" rows="5" class="form-control" placeholder="If this were a tweet how would you describe your event? (keep it short)"></textarea>
                                <small class="form-text text-muted">*Maximum 120 characters</small>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>START DATE*</label>
                                    <div class="icon-input-control">
                                        <i class="fa fa-calendar-alt"></i>
                                        <input id="createEventFromDate" type="text" class="form-control" placeholder="Please enter date">
                                    </div>

                                </div>
                                <div class="form-group col-md-6">
                                    <label>START TIME*</label>
                                    <div class="icon-input-control">
                                        <i class="fa fa-clock"></i>
                                        <input id="createEventFromTime" type="text" class="form-control" placeholder="Please enter time">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>END DATE*</label>
                                    <div class="icon-input-control">
                                        <i class="fa fa-calendar-alt"></i>
                                        <input id="createEventToDate" type="text" class="form-control" placeholder="Please enter date">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>END TIME*</label>
                                    <div class="icon-input-control">
                                        <i class="fa fa-clock"></i>
                                        <input id="createEventToTime" type="text" class="form-control" placeholder="Please enter time">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>TICKET TYPE & PRICE*</label>
                                <small class="form-text text-muted">*Maximum 120 characters</small>
                                <div class="ticket-types-container">
                                    <div class="row align-items-center my-2">
                                        <div class="col-md-4">
                                            <input id="ticket-type" type="text" class="form-control" placeholder="Ticket type">
                                        </div>
                                        <div class="col-md-4">
                                            <input id="price" type="number" class="form-control" placeholder="Price QR">
                                        </div>
                                        <div class="col-md-4 d-flex">
                                            <div class="custom-control custom-checkbox">
                                                <input id="itsFreeCheck" type="checkbox" class="custom-control-input">
                                                <label for="itsFreeCheck" class="custom-control-label">IT'S FREE!</label>
                                            </div>
                                            <a href="#" class="removeTicketTypes ml-3"><i class="fas fa-trash-alt"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <button id="addTicketType" type="button" class="btn btn-pink text-uppercase mt-3"><i class="fa fa-plus mr-2"></i> Add Ticket Type</button>
                            </div>
                            <div class="form-group">
                                <label>ORGANISER*</label>
                                <input id="organiser" type="text" class="form-control" placeholder="Organizer">
                            </div>
                            <div class="form-group">
                                <label>TICKET PURCHASE*</label><br>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input id="noNeedOption" type="checkbox" class="custom-control-input" name="ticketPurchaseOptions">
                                    <label for="noNeedOption" class="custom-control-label">No tickets needed</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input id="atDoorOption" type="checkbox" class="custom-control-input" name="ticketPurchaseOptions">
                                    <label for="atDoorOption" class="custom-control-label">At the door</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input id="shopsCheck" type="checkbox" class="custom-control-input" name="ticketPurchaseOptions">
                                    <label for="shopsCheck" class="custom-control-label">Shops</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input id="onlineCheck" type="checkbox" class="custom-control-input" name="ticketPurchaseOptions">
                                    <label for="onlineCheck" class="custom-control-label">Online</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>BODY*</label>
                                <textarea id="body" rows="6" class="form-control"></textarea>
                            </div>
                            <div class="form-group location-form-group">
                                <label>LOCATION*</label>
                                <input type="hidden" id="latitude" value="25.2838208">
                                <input type="hidden" id="longitude" value="51.491357900000025">
                                <input type="hidden" id="street">
                                <input id="address" type="text" class="form-control" placeholder="Address">
                                <div id="map_canvas" style="height: 500px;"></div>
                            </div>
                            <div class="form-group">
                                <label>CONTACT INFORMATION*</label>
                                <input id="phone" type="text" class="form-control mb-1" placeholder="Phone">
                                <input id="email" type="email" class="form-control mb-1" placeholder="Email">
                                <input type="url" class="form-control" placeholder="Website">
                            </div>
                            <div class="form-group">
                                <label>TAGS*</label>
                                <input id="tags" type="text" class="form-control">
                                <small class="form-text text-muted">*Separate each tag with a space</small>
                            </div>
                            <div class="row">
                                <button class="col-md-4 offset-md-4 btn btn-pink btn-block">Send</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <aside class="side-area">
                            <a href="#" class="btn btn-grey btn-lg btn-block btn-rounded mb-4 btn-icon-right">Submit Events <i class="fa fa-arrow-right"></i></a>
                            <div class="advertisement-widget">
                                <img class="img-fluid" src="./assets/images/dummy/495712436105197116.jpg" alt="">
                            </div>
                            <div class="widget calendar-widget">
                                <div id="eventsDatePicker"></div>
                            </div>
                            <div class="advertisement-widget">
                                <img class="img-fluid" src="./assets/images/dummy/1841746877016134826.jpg" alt="">
                            </div>
                            <div class="widget search-filter-widget">
                                <form>
                                    <div class="accordion" id="eventsFilter">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h2>
                                                    <button class="btn btn-link collapsed text-uppercase" type="button" data-toggle="collapse" data-target="#collapseEvents" aria-expanded="true"
                                                            aria-controls="collapseOne">
                                                        Events Filter <i class="fa fa-angle-down mt-2 float-right"></i>
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseEvents" class="collapse" aria-labelledby="headingOne" data-parent="#eventsFilter">
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label>Date</label>
                                                        <input id="from_date" type="text" class="form-control" placeholder="From">
                                                    </div>
                                                    <div class="form-group">
                                                        <input id="to_date" type="text" class="form-control" placeholder="To">
                                                    </div>
                                                    <div class="filter-checkboxes">
                                                        <label class="text-uppercase">Categories</label>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input " id="customControlAutosizing1">
                                                            <label class="custom-control-label entertainment" for="customControlAutosizing1">Entertainment</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input " id="customControlAutosizing2">
                                                            <label class="custom-control-label artsCulture" for="customControlAutosizing2">Arts & Culture</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing3">
                                                            <label class="custom-control-label sports" for="customControlAutosizing3">Sports</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing4">
                                                            <label class="custom-control-label foodDining" for="customControlAutosizing4">Food & Dining</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing5">
                                                            <label class="custom-control-label other" for="customControlAutosizing5">Other</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing6">
                                                            <label class="custom-control-label community" for="customControlAutosizing6">Community </label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing7">
                                                            <label class="custom-control-label night" for="customControlAutosizing7">Night</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing8">
                                                            <label class="custom-control-label socialResoponsibility" for="customControlAutosizing8">SocialResoponsibility</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing9">
                                                            <label class="custom-control-label education" for="customControlAutosizing9">Education</label>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-purple btn-block mt-4">Yalla!</button>
                                                    <button type="button" class="btn btn-pink btn-block">Reset Filter</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="advertisement-widget sticky-widget">
                                <img class="img-fluid" src="./assets/images/dummy/1013443136016165343.png" alt="">
                            </div>
                        </aside>
                    </div>
                </div>
            </section>
        </div>
    </main>
<?php include_once "footer.php" ?>