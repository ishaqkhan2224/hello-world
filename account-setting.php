<?php include_once "header.php" ?>
    <main class="main bg-darkgray">
        <div class="container bg-light">
            <section class="section">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <?php require "./inc/advertisement.php" ?>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <h1 class="form-title mb-4">Account settings</h1>
                        <form action="/" class="edit-profile-form">
                            <h2>Change password</h2>
                            <div class="row mt-4">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Your current password</label>
                                        <input type="password" class="form-control" placeholder="Please enter your current password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Type a new password</label>
                                        <input type="password" class="form-control" placeholder="Please type a new password">
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-pink btn-sharp mt-5 btn-md" type="submit">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </main>
<?php include_once "footer.php" ?>