<?php include_once "header.php" ?>
    <main class="main bg-darkgray">
        <div class="container">
            <section class="section bg-light">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <?php require "./inc/advertisement.php" ?>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <h1 class="form-title mb-4">Edit Profile</h1>
                        <form action="/" class="edit-profile-form">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="profile-img">
                                        <div class="select-file">
                                          <span>
                                            <i class="fas fa-camera"></i>
                                          </span>
                                            <input type="file" class="form-control-file">
                                        </div>
                                        <img src="https://via.placeholder.com/160x160&text=Avatar" alt="">
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Username: </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">E-mail Address:</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Time Zone:</label>
                                        <div class="col-sm-9">
                                            <select class="form-control">
                                                <option value="">Current time zone</option>
                                                <option value="-12" selected="">(GMT-12:00) International Date Line West</option>
                                                <option value="-11">(GMT-11:00) Midway Island, Samoa</option>
                                                <option value="-10">(GMT-10:00) Hawaii</option>
                                                <option value="-9">(GMT-09:00) Alaska</option>
                                                <option value="-8">(GMT-08:00) Pacific Time (US &amp; Canada)</option>
                                                <option value="-8">(GMT-08:00) Tijuana, Baja California</option>
                                                <option value="-7">(GMT-07:00) Arizona</option>
                                                <option value="-7">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
                                                <option value="-7">(GMT-07:00) Mountain Time (US &amp; Canada)</option>
                                                <option value="-6">(GMT-06:00) Central America</option>
                                                <option value="-6">(GMT-06:00) Central Time (US &amp; Canada)</option>
                                                <option value="-6">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                                <option value="-6">(GMT-06:00) Saskatchewan</option>
                                                <option value="-5">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                                <option value="-5">(GMT-05:00) Eastern Time (US &amp; Canada)</option>
                                                <option value="-5">(GMT-05:00) Indiana (East)</option>
                                                <option value="-4">(GMT-04:00) Atlantic Time (Canada)</option>
                                                <option value="-4">(GMT-04:00) Caracas, La Paz</option>
                                                <option value="-4">(GMT-04:00) Manaus</option>
                                                <option value="-4">(GMT-04:00) Santiago</option>
                                                <option value="-3.5">(GMT-03:30) Newfoundland</option>
                                                <option value="-3">(GMT-03:00) Brasilia</option>
                                                <option value="-3">(GMT-03:00) Buenos Aires, Georgetown</option>
                                                <option value="-3">(GMT-03:00) Greenland</option>
                                                <option value="-3">(GMT-03:00) Montevideo</option>
                                                <option value="-2">(GMT-02:00) Mid-Atlantic</option>
                                                <option value="-1">(GMT-01:00) Cape Verde Is.</option>
                                                <option value="-1">(GMT-01:00) Azores</option>
                                                <option value="0">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>
                                                <option value="0">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>
                                                <option value="1">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                                <option value="1">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                                <option value="1">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                                <option value="1">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
                                                <option value="1">(GMT+01:00) West Central Africa</option>
                                                <option value="2">(GMT+02:00) Amman</option>
                                                <option value="2">(GMT+02:00) Athens, Bucharest, Istanbul</option>
                                                <option value="2">(GMT+02:00) Beirut</option>
                                                <option value="2">(GMT+02:00) Cairo</option>
                                                <option value="2">(GMT+02:00) Harare, Pretoria</option>
                                                <option value="2">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
                                                <option value="2">(GMT+02:00) Jerusalem</option>
                                                <option value="2">(GMT+02:00) Minsk</option>
                                                <option value="2">(GMT+02:00) Windhoek</option>
                                                <option value="3">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>
                                                <option value="3">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                                <option value="3">(GMT+03:00) Nairobi</option>
                                                <option value="3">(GMT+03:00) Tbilisi</option>
                                                <option value="3.5">(GMT+03:30) Tehran</option>
                                                <option value="4">(GMT+04:00) Abu Dhabi, Muscat</option>
                                                <option value="4">(GMT+04:00) Baku</option>
                                                <option value="4">(GMT+04:00) Yerevan</option>
                                                <option value="4.5">(GMT+04:30) Kabul</option>
                                                <option value="5">(GMT+05:00) Yekaterinburg</option>
                                                <option value="5">(GMT+05:00) Islamabad, Karachi, Tashkent</option>
                                                <option value="5.5">(GMT+05:30) Sri Jayawardenapura</option>
                                                <option value="5.5">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                                <option value="5.75">(GMT+05:45) Kathmandu</option>
                                                <option value="6">(GMT+06:00) Almaty, Novosibirsk</option>
                                                <option value="6">(GMT+06:00) Astana, Dhaka</option>
                                                <option value="6.5">(GMT+06:30) Yangon (Rangoon)</option>
                                                <option value="7">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                                <option value="7">(GMT+07:00) Krasnoyarsk</option>
                                                <option value="8">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                                <option value="8">(GMT+08:00) Kuala Lumpur, Singapore</option>
                                                <option value="8">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                                <option value="8">(GMT+08:00) Perth</option>
                                                <option value="8">(GMT+08:00) Taipei</option>
                                                <option value="9">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                                <option value="9">(GMT+09:00) Seoul</option>
                                                <option value="9">(GMT+09:00) Yakutsk</option>
                                                <option value="9.5">(GMT+09:30) Adelaide</option>
                                                <option value="9.5">(GMT+09:30) Darwin</option>
                                                <option value="10">(GMT+10:00) Brisbane</option>
                                                <option value="10">(GMT+10:00) Canberra, Melbourne, Sydney</option>
                                                <option value="10">(GMT+10:00) Hobart</option>
                                                <option value="10">(GMT+10:00) Guam, Port Moresby</option>
                                                <option value="10">(GMT+10:00) Vladivostok</option>
                                                <option value="11">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>
                                                <option value="12">(GMT+12:00) Auckland, Wellington</option>
                                                <option value="12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                                <option value="13">(GMT+13:00) Nuku'alofa</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-md-6">
                                    <h2 class="form-title">General</h2>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">First name </label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="text" class="form-control" placeholder="Please enter your first name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Last name</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="text" class="form-control" placeholder="Please enter your last name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Birthday</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="date" class="form-control" placeholder="Choose date of Birthday">
                                        </div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-3 col-form-label">Gender</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="maleOption" name="genderOptions" class="custom-control-input">
                                                <label class="custom-control-label" for="maleOption">Male</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="femaleOption" name="genderOptions" class="custom-control-input">
                                                <label class="custom-control-label" for="femaleOption">Female</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Country</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <select class="form-control">
                                                <option value="">Choose your country</option>
                                                <option value="AF" selected="">Afghanistan</option>
                                                <option value="AX">Aland Islands</option>
                                                <option value="AL">Albania</option>
                                                <option value="DZ">Algeria</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AD">Andorra</option>
                                                <option value="AO">Angola</option>
                                                <option value="AI">Anguilla</option>
                                                <option value="AQ">Antarctica</option>
                                                <option value="AG">Antigua and Barbuda</option>
                                                <option value="AR">Argentina</option>
                                                <option value="AM">Armenia</option>
                                                <option value="AW">Aruba</option>
                                                <option value="AU">Australia</option>
                                                <option value="AT">Austria</option>
                                                <option value="AZ">Azerbaijan</option>
                                                <option value="BS">Bahamas</option>
                                                <option value="BH">Bahrain</option>
                                                <option value="BD">Bangladesh</option>
                                                <option value="BB">Barbados</option>
                                                <option value="BY">Belarus</option>
                                                <option value="BE">Belgium</option>
                                                <option value="BZ">Belize</option>
                                                <option value="BJ">Benin</option>
                                                <option value="BM">Bermuda</option>
                                                <option value="BT">Bhutan</option>
                                                <option value="BO">Bolivia</option>
                                                <option value="BA">Bosnia and Herzegovina</option>
                                                <option value="BW">Botswana</option>
                                                <option value="BV">Bouvet Island</option>
                                                <option value="BR">Brazil</option>
                                                <option value="IO">British Indian Ocean Territory</option>
                                                <option value="BN">Brunei Darussalam</option>
                                                <option value="BG">Bulgaria</option>
                                                <option value="BF">Burkina Faso</option>
                                                <option value="BI">Burundi</option>
                                                <option value="KH">Cambodia</option>
                                                <option value="CM">Cameroon</option>
                                                <option value="CA">Canada</option>
                                                <option value="CV">Cape Verde</option>
                                                <option value="KY">Cayman Islands</option>
                                                <option value="CF">Central African Republic</option>
                                                <option value="TD">Chad</option>
                                                <option value="CL">Chile</option>
                                                <option value="CN">China</option>
                                                <option value="CX">Christmas Island</option>
                                                <option value="CC">Cocos (Keeling) Islands</option>
                                                <option value="CO">Colombia</option>
                                                <option value="KM">Comoros</option>
                                                <option value="CG">Congo</option>
                                                <option value="CD">Congo, The Democratic Republic of the</option>
                                                <option value="CK">Cook Islands</option>
                                                <option value="CR">Costa Rica</option>
                                                <option value="CI">Cote D'Ivoire</option>
                                                <option value="HR">Croatia</option>
                                                <option value="CU">Cuba</option>
                                                <option value="CY">Cyprus</option>
                                                <option value="CZ">Czech Republic</option>
                                                <option value="DK">Denmark</option>
                                                <option value="DJ">Djibouti</option>
                                                <option value="DM">Dominica</option>
                                                <option value="DO">Dominican Republic</option>
                                                <option value="EC">Ecuador</option>
                                                <option value="EG">Egypt</option>
                                                <option value="SV">El Salvador</option>
                                                <option value="GQ">Equatorial Guinea</option>
                                                <option value="ER">Eritrea</option>
                                                <option value="EE">Estonia</option>
                                                <option value="ET">Ethiopia</option>
                                                <option value="FK">Falkland Islands (Malvinas)</option>
                                                <option value="FO">Faroe Islands</option>
                                                <option value="FJ">Fiji</option>
                                                <option value="FI">Finland</option>
                                                <option value="FR">France</option>
                                                <option value="GF">French Guiana</option>
                                                <option value="PF">French Polynesia</option>
                                                <option value="TF">French Southern Territories</option>
                                                <option value="GA">Gabon</option>
                                                <option value="GM">Gambia</option>
                                                <option value="GE">Georgia</option>
                                                <option value="DE">Germany</option>
                                                <option value="GH">Ghana</option>
                                                <option value="GI">Gibraltar</option>
                                                <option value="GR">Greece</option>
                                                <option value="GL">Greenland</option>
                                                <option value="GD">Grenada</option>
                                                <option value="GP">Guadeloupe</option>
                                                <option value="GU">Guam</option>
                                                <option value="GT">Guatemala</option>
                                                <option value="GG">Guernsey</option>
                                                <option value="GN">Guinea</option>
                                                <option value="GW">Guinea-Bissau</option>
                                                <option value="GY">Guyana</option>
                                                <option value="HT">Haiti</option>
                                                <option value="HM">Heard Island and Mcdonald Islands</option>
                                                <option value="VA">Holy See (Vatican City State)</option>
                                                <option value="HN">Honduras</option>
                                                <option value="HK">Hong Kong</option>
                                                <option value="HU">Hungary</option>
                                                <option value="IS">Iceland</option>
                                                <option value="IN">India</option>
                                                <option value="ID">Indonesia</option>
                                                <option value="IR">Iran, Islamic Republic Of</option>
                                                <option value="IQ">Iraq</option>
                                                <option value="IE">Ireland</option>
                                                <option value="IM">Isle of Man</option>
                                                <option value="IL">Israel</option>
                                                <option value="IT">Italy</option>
                                                <option value="JM">Jamaica</option>
                                                <option value="JP">Japan</option>
                                                <option value="JE">Jersey</option>
                                                <option value="JO">Jordan</option>
                                                <option value="KZ">Kazakhstan</option>
                                                <option value="KE">Kenya</option>
                                                <option value="KI">Kiribati</option>
                                                <option value="KP">Korea, Democratic People's Republic of</option>
                                                <option value="KR">Korea, Republic of</option>
                                                <option value="KW">Kuwait</option>
                                                <option value="KG">Kyrgyzstan</option>
                                                <option value="LA">Lao People's Democratic Republic</option>
                                                <option value="LV">Latvia</option>
                                                <option value="LB">Lebanon</option>
                                                <option value="LS">Lesotho</option>
                                                <option value="LR">Liberia</option>
                                                <option value="LY">Libyan Arab Jamahiriya</option>
                                                <option value="LI">Liechtenstein</option>
                                                <option value="LT">Lithuania</option>
                                                <option value="LU">Luxembourg</option>
                                                <option value="MO">Macao</option>
                                                <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                                <option value="MG">Madagascar</option>
                                                <option value="MW">Malawi</option>
                                                <option value="MY">Malaysia</option>
                                                <option value="MV">Maldives</option>
                                                <option value="ML">Mali</option>
                                                <option value="MT">Malta</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MQ">Martinique</option>
                                                <option value="MR">Mauritania</option>
                                                <option value="MU">Mauritius</option>
                                                <option value="YT">Mayotte</option>
                                                <option value="MX">Mexico</option>
                                                <option value="FM">Micronesia, Federated States of</option>
                                                <option value="MD">Moldova, Republic of</option>
                                                <option value="MC">Monaco</option>
                                                <option value="MN">Mongolia</option>
                                                <option value="MS">Montserrat</option>
                                                <option value="MA">Morocco</option>
                                                <option value="MZ">Mozambique</option>
                                                <option value="MM">Myanmar</option>
                                                <option value="NA">Namibia</option>
                                                <option value="NR">Nauru</option>
                                                <option value="NP">Nepal</option>
                                                <option value="NL">Netherlands</option>
                                                <option value="AN">Netherlands Antilles</option>
                                                <option value="NC">New Caledonia</option>
                                                <option value="NZ">New Zealand</option>
                                                <option value="NI">Nicaragua</option>
                                                <option value="NE">Niger</option>
                                                <option value="NG">Nigeria</option>
                                                <option value="NU">Niue</option>
                                                <option value="NF">Norfolk Island</option>
                                                <option value="MP">Northern Mariana Islands</option>
                                                <option value="NO">Norway</option>
                                                <option value="OM">Oman</option>
                                                <option value="PK">Pakistan</option>
                                                <option value="PW">Palau</option>
                                                <option value="PS">Palestinian Territory, Occupied</option>
                                                <option value="PA">Panama</option>
                                                <option value="PG">Papua New Guinea</option>
                                                <option value="PY">Paraguay</option>
                                                <option value="PE">Peru</option>
                                                <option value="PH">Philippines</option>
                                                <option value="PN">Pitcairn</option>
                                                <option value="PL">Poland</option>
                                                <option value="PT">Portugal</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="QA">Qatar</option>
                                                <option value="RE">Reunion</option>
                                                <option value="RO">Romania</option>
                                                <option value="RU">Russian Federation</option>
                                                <option value="RW">Rwanda</option>
                                                <option value="SH">Saint Helena</option>
                                                <option value="KN">Saint Kitts and Nevis</option>
                                                <option value="LC">Saint Lucia</option>
                                                <option value="PM">Saint Pierre and Miquelon</option>
                                                <option value="VC">Saint Vincent and the Grenadines</option>
                                                <option value="WS">Samoa</option>
                                                <option value="SM">San Marino</option>
                                                <option value="ST">Sao Tome and Principe</option>
                                                <option value="SA">Saudi Arabia</option>
                                                <option value="SN">Senegal</option>
                                                <option value="CS">Serbia and Montenegro</option>
                                                <option value="SC">Seychelles</option>
                                                <option value="SL">Sierra Leone</option>
                                                <option value="SG">Singapore</option>
                                                <option value="SK">Slovakia</option>
                                                <option value="SI">Slovenia</option>
                                                <option value="SB">Solomon Islands</option>
                                                <option value="SO">Somalia</option>
                                                <option value="ZA">South Africa</option>
                                                <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                <option value="ES">Spain</option>
                                                <option value="LK">Sri Lanka</option>
                                                <option value="SD">Sudan</option>
                                                <option value="SR">Suriname</option>
                                                <option value="SJ">Svalbard and Jan Mayen</option>
                                                <option value="SZ">Swaziland</option>
                                                <option value="SE">Sweden</option>
                                                <option value="CH">Switzerland</option>
                                                <option value="SY">Syrian Arab Republic</option>
                                                <option value="TW">Taiwan, Province of China</option>
                                                <option value="TJ">Tajikistan</option>
                                                <option value="TZ">Tanzania, United Republic of</option>
                                                <option value="TH">Thailand</option>
                                                <option value="TL">Timor-Leste</option>
                                                <option value="TG">Togo</option>
                                                <option value="TK">Tokelau</option>
                                                <option value="TO">Tonga</option>
                                                <option value="TT">Trinidad and Tobago</option>
                                                <option value="TN">Tunisia</option>
                                                <option value="TR">Turkey</option>
                                                <option value="TM">Turkmenistan</option>
                                                <option value="TC">Turks and Caicos Islands</option>
                                                <option value="TV">Tuvalu</option>
                                                <option value="UG">Uganda</option>
                                                <option value="UA">Ukraine</option>
                                                <option value="AE">United Arab Emirates</option>
                                                <option value="GB">United Kingdom</option>
                                                <option value="US">United States</option>
                                                <option value="UM">United States Minor Outlying Islands</option>
                                                <option value="UY">Uruguay</option>
                                                <option value="UZ">Uzbekistan</option>
                                                <option value="VU">Vanuatu</option>
                                                <option value="VE">Venezuela</option>
                                                <option value="VN">Viet Nam</option>
                                                <option value="VG">Virgin Islands, British</option>
                                                <option value="VI">Virgin Islands, U.S.</option>
                                                <option value="WF">Wallis and Futuna</option>
                                                <option value="EH">Western Sahara</option>
                                                <option value="YE">Yemen</option>
                                                <option value="ZM">Zambia</option>
                                                <option value="ZW">Zimbabwe</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Phone</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="number" class="form-control" placeholder="Please enter your phone number">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Bio</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="text" class="form-control" placeholder="Please enter your job title">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h2 class="form-title">Your external links</h2>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Facebook</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="text" class="form-control" placeholder="Paste your link here">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Google+</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="text" class="form-control" placeholder="Paste your link here">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Twitter</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="text" class="form-control" placeholder="Paste your link here">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Youtube</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="text" class="form-control" placeholder="Paste your link here">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Instagram</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="text" class="form-control" placeholder="Paste your link here">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Snapchat</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="text" class="form-control" placeholder="Paste your link here">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">LinkedIn</label>
                                        <div class="col-sm-9 pl-sm-0">
                                            <input type="text" class="form-control" placeholder="Paste your link here">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-8 offset-md-2">
                                    <div class="row">
                                        <label class="col-sm-4 col-form-label">Current Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" placeholder="Type your current password">
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-pink btn-sharp mt-5 btn-md" type="submit">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </main>
<?php include_once "footer.php" ?>