<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Qatar Events - What's happening, BEFORE it happens!</title>
    <meta content="All about Qatar's events! Grab the latest events happening in Qatar." name="description">
    <meta content="" name="keywords">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,  user-scalable=no">
    <meta content="telephone=no" name="format-detection">
    <meta name="HandheldFriendly" content="true">

    <!--[if (gt IE 9)|!(IE)]><!-->
    <link href="./assets/css/main.min.css" rel="stylesheet" type="text/css">
    <!--<![endif]-->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:url" content="http://www.qatarevents.co/">
    <meta name="twitter:title" content="Qatar Events - What's happening, BEFORE it happens!">
    <meta name="twitter:description" content="All about Qatar's events! Grab the latest events happening in Qatar.">
    <meta name="twitter:image:src" content="http://www.qatarevents.co/static/images/content/preview.jpg">
    <meta property="og:title" content="Qatar Events - What's happening, BEFORE it happens!">
    <meta property="og:type" content="article">
    <meta property="og:image" content="http://www.qatarevents.co/static/images/content/preview.jpg">
    <meta property="og:url" content="http://www.qatarevents.co/">
    <meta property="og:description" content="All about Qatar's events! Grab the latest events happening in Qatar.">
    <meta itemprop="name" content="Qatar Events - What's happening, BEFORE it happens!">
    <meta itemprop="description" content="All about Qatar's events! Grab the latest events happening in Qatar.">
    <meta itemprop="image" content="http://www.qatarevents.co/static/images/content/preview.jpg">
    <link rel="image_src" href="http://www.qatarevents.co/static/images/content/preview.jpg">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <script>
        (function (H) {
            H.className = H.className.replace(/\bno-js\b/, 'js')
        })(document.documentElement)
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '181351492725518');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=181351492725518&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122498944-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-122498944-2');
    </script>
    <!-- End - Google Analytics -->
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TMFNLCQ');</script>
    <!-- End Google Tag Manager -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css?ver=5.7.2">
    <link rel="stylesheet" href="./assets/css/lib/bootstrap.4.3.1.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="./assets/js/lib/owlCarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="./assets/css/custom.css">
    <link rel="stylesheet" href="./assets/css/responsive.css">

    <script src="./assets/js/lib/moment.min.js"></script>
</head>

<body>
<header class="main-header sticky">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark main-navbar">
            <a class="navbar-brand" href="#"><img src="./assets/images/assets/footer/logo-qe.png" class="img-fluid" alt=""></a>
            <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Events</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#register_form_modal">Register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#login_form_modal">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#contact_us_form_modal">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="btn nav-btn btn-white"><i class="icon-qe mr-2"></i> Submit Event</a>
                    </li>
                </ul>
                <form action="/" class="search-form form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <i class="fa fa-search"></i>
                    </div>
                </form>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#login_form_modal">Register/Login</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
