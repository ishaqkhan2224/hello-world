<?php include_once "header.php"; ?>
    <section class="hero-section sub-hero">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-md-7">
                    <h1>just like you...</h1>
                    <p>we were tired of always hearing about <br>events after they were done.</p>
                </div>
                <div class="col-md-4">
                    <div class="g-logo-img">
                        <img class="g-logo" src="./assets/images/eq.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <main class="bg-darkgray">
        <div class="container bg-light">
            <section class="features-section">
                <div class="row align-items-center">
                    <div class="col-md-6 order-md-last">
                        <div class="feature-img">
                            <img class="img-fluid" src="./assets/images/f1.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content left">
                            <h1>HOMEPAGE</h1>
                            <p>With Qatar events app, you'll never be the person who is not updated with all the cool stuffs happening in Qatar. Numerous events listed and updated daily to keep you informed! So, stay
                                tuned!</p>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="feature-img">
                            <img class="img-fluid" src="./assets/images/f2.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-5 offset-md-1">
                        <div class="content right">
                            <h1>LOGIN</h1>
                            <p>Already registered on ILoveQatar? Login with your existing account to get full access to the features of the app! If not, you can use your Facebook or twitter to login.</p>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6 order-md-last">
                        <div class="feature-img">
                            <img class="img-fluid" src="./assets/images/f3.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content left">
                            <h1>FILTERS</h1>
                            <p>You can use the three most intuitive filters. Today, this week and Later to filter the events. Or, you can filter events based on event type and dates!</p>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="feature-img">
                            <img class="img-fluid" src="./assets/images/f4.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-5 offset-md-1">
                        <div class="content right">
                            <h1>CHANNELS</h1>
                            <p>Have a favorite event organizer? Fan of a brand in Qatar? Check out the channels list to find and subscribe your favorite events by them!</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>

<?php include_once "footer.php"; ?>