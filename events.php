<?php include_once "header.php" ?>
    <div class="sub-menu bg-darkgray">
        <div class="container bg-light">
            <nav class="sub-menu-nav text-center">
                <ul>
                    <li><a class="menu-item-blue" href="#">Entertainment</a></li>
                    <li><a class="menu-item-skyblue active" href="#">Arts & Culture</a></li>
                    <li><a class="menu-item-darkgreen" href="#">Social Responsibility</a></li>
                    <li><a class="menu-item-green" href="#">Sports</a></li>
                    <li><a class="menu-item-yellow" href="#">Food & Dining</a></li>
                    <li><a class="menu-item-purpale" href="#">Community</a></li>
                    <li><a class="menu-item-darkbrown" href="#">Education</a></li>
                    <li><a class="menu-item-darkpurpale" href="#">Night</a></li>
                    <li><a class="menu-item-darkpurpale" href="#">Volunteer</a></li>
                    <li><a class="menu-item-orange" href="#">Other</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <main class="main bg-darkgray">
        <div class="container bg-light">
            <section class="section">
                <div class="row">
                    <div class="col-md-9">
                        <?php require "./inc/advertisement.php" ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-lg-9">
                        <h1 class="mb-4">Events</h1>
                        <div class="owl-carousel events-carousel">
                            <div class="event-item">
                                <a href="#" class="d-block"><img src="./assets/images/carousel-img1.png" class="img-fluid mb-4"></a>
                                <a href="#"><h5 class="text-white">Title here</h5></a>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat inventore at dolore, impedit nemo quia incidunt architecto, doloribus alias quidem quibusdam dolores suscipit
                                    laborum illum tempora blanditiis minima repellendus perferendis?</p>
                                <a href="#" class="btn read-more-btn">Read more</a>
                                <span>Categories Arts & Culture</span>
                            </div>
                            <div class="event-item">
                                <a href="#" class="d-block"><img src="./assets/images/carousel-img1.png" class="img-fluid mb-4"></a>
                                <a href="#"><h5 class="text-white">Title here</h5></a>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat inventore at dolore, impedit nemo quia incidunt architecto, doloribus alias quidem quibusdam dolores suscipit
                                    laborum illum tempora blanditiis minima repellendus perferendis?</p>
                                <a href="#" class="btn read-more-btn">Read more</a>
                                <span>Categories Arts & Culture</span>
                            </div>
                            <div class="event-item">
                                <a href="#" class="d-block"><img src="./assets/images/carousel-img1.png" class="img-fluid mb-4"></a>
                                <a href="#"><h5 class="text-white">Title here</h5></a>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat inventore at dolore, impedit nemo quia incidunt architecto, doloribus alias quidem quibusdam dolores suscipit
                                    laborum illum tempora blanditiis minima repellendus perferendis?</p>
                                <a href="#" class="btn read-more-btn">Read more</a>
                                <span>Categories Arts & Culture</span>
                            </div>
                        </div>
                        <hr>
                        <ul class="latest-events mb-5 mt-5">
                            <li><a class="text-blue" href="#">Today Onwords</a></li>
                            <li><a class="text-blue" href="#">This Weekend</a></li>
                            <li><a class="text-blue" href="#">This Month</a></li>
                        </ul>
                        <div class="row">
                            <div class="col-md-6 mb-5">
                                <div class="event-box">
                                    <a href="#" class="d-block">
                                        <img src="./assets/images/event-1.png" alt="" class="img-fluid">
                                    </a>
                                    <div class="box-body">
                                        <a href="#"><h4>Lorem ipsum dolor sit non, magni quidem laborum.</h4></a>
                                        <hr>
                                        <div class="media location mb-2">
                                            <i class="fas fa-map-marker-alt"></i>
                                            <div class="media-body">
                                                Cras sit amet nibh lientum nunc ac nisi vulputate fringilla.
                                            </div>
                                        </div>
                                        <div class="row m-0 mb-3">
                                            <div class="col-md-12 p-0 mb-2">
                                                <i class="fas fa-calendar-alt mr-2"></i>
                                                <span>5 Sep, 2019</span>
                                            </div>
                                            <div class="col-md-12 p-0">
                                                <i class="far fa-clock mr-2"></i>
                                                <span> 4 pm-7:00 pm</span>
                                            </div>
                                        </div>
                                        <span>Categories: Community</span>
                                        <hr>
                                        <a href="#" class="btn btn-primary event-submit-btn bg-orange">Buy Tickets/Free Events</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-5">
                                <div class="event-box">
                                    <a href="#" class="d-block">
                                        <img src="./assets/images/event-1.png" alt="" class="img-fluid">
                                    </a>
                                    <div class="box-body">
                                        <a href="#"><h4>Lorem ipsum dolor sit non, magni quidem laborum.</h4></a>
                                        <hr>
                                        <div class="media location mb-2">
                                            <i class="fas fa-map-marker-alt"></i>
                                            <div class="media-body">
                                                Cras sit amet nibh lientum nunc ac nisi vulputate fringilla.
                                            </div>
                                        </div>
                                        <div class="row m-0 mb-3">
                                            <div class="col-md-12 p-0 mb-2">
                                                <i class="fas fa-calendar-alt mr-2"></i>
                                                <span>5 Sep, 2019</span>
                                            </div>
                                            <div class="col-md-12 p-0">
                                                <i class="far fa-clock mr-2"></i>
                                                <span> 4 pm-7:00 pm</span>
                                            </div>
                                        </div>
                                        <span>Categories: Community</span>
                                        <hr>
                                        <a href="#" class="btn btn-primary event-submit-btn bg-orange">Buy Tickets/Free Events</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-5">
                                <div class="event-box">
                                    <a href="#" class="d-block">
                                        <img src="./assets/images/event-item-2.png" alt="" class="img-fluid">
                                    </a>
                                    <div class="box-body">
                                        <a href="#"><h4>Lorem ipsum dolor sit non, magni quidem laborum.</h4></a>
                                        <hr>
                                        <div class="media location mb-2">
                                            <i class="fas fa-map-marker-alt"></i>
                                            <div class="media-body">
                                                Cras sit amet nibh lientum nunc ac nisi vulputate fringilla.
                                            </div>
                                        </div>
                                        <div class="row m-0 mb-3">
                                            <div class="col-md-12 p-0 mb-2">
                                                <i class="fas fa-calendar-alt mr-2"></i>
                                                <span>5 Sep, 2019</span>
                                            </div>
                                            <div class="col-md-12 p-0">
                                                <i class="far fa-clock mr-2"></i>
                                                <span> 4 pm-7:00 pm</span>
                                            </div>
                                        </div>
                                        <span>Categories: Community</span>
                                        <hr>
                                        <a href="#" class="btn btn-primary event-submit-btn bg-orange">Buy Tickets/Free Events</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-5">
                                <div class="event-box">
                                    <a href="#" class="d-block">
                                        <img src="./assets/images/event-item-1.png" alt="" class="img-fluid">
                                    </a>
                                    <div class="box-body">
                                        <a href="#"><h4>Lorem ipsum dolor sit non, magni quidem laborum.</h4></a>
                                        <hr>
                                        <div class="media location mb-2">
                                            <i class="fas fa-map-marker-alt"></i>
                                            <div class="media-body">
                                                Cras sit amet nibh lientum nunc ac nisi vulputate fringilla.
                                            </div>
                                        </div>
                                        <div class="row m-0 mb-3">
                                            <div class="col-md-12 p-0 mb-2">
                                                <i class="fas fa-calendar-alt mr-2"></i>
                                                <span>5 Sep, 2019</span>
                                            </div>
                                            <div class="col-md-12 p-0">
                                                <i class="far fa-clock mr-2"></i>
                                                <span> 4 pm-7:00 pm</span>
                                            </div>
                                        </div>
                                        <span>Categories: Community</span>
                                        <hr>
                                        <a href="#" class="btn btn-primary event-submit-btn bg-red">Buy Tickets/Free Events</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-5">
                                <div class="event-box">
                                    <a href="#" class="d-block">
                                        <img src="./assets/images/event-item-1.png" alt="" class="img-fluid">
                                    </a>
                                    <div class="box-body">
                                        <a href="#"><h4>Lorem ipsum dolor sit non, magni quidem laborum.</h4></a>
                                        <hr>
                                        <div class="media location mb-2">
                                            <i class="fas fa-map-marker-alt"></i>
                                            <div class="media-body">
                                                Cras sit amet nibh lientum nunc ac nisi vulputate fringilla.
                                            </div>
                                        </div>
                                        <div class="row m-0 mb-3">
                                            <div class="col-md-12 p-0 mb-2">
                                                <i class="fas fa-calendar-alt mr-2"></i>
                                                <span>5 Sep, 2019</span>
                                            </div>
                                            <div class="col-md-12 p-0">
                                                <i class="far fa-clock mr-2"></i>
                                                <span> 4 pm-7:00 pm</span>
                                            </div>
                                        </div>
                                        <span>Categories: Community</span>
                                        <hr>
                                        <a href="#" class="btn btn-primary event-submit-btn bg-green">Buy Tickets/Free Events</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-5">
                                <div class="event-box">
                                    <a href="#" class="d-block">
                                        <img src="./assets/images/event-item-3.png" alt="" class="img-fluid">
                                    </a>
                                    <div class="box-body">
                                        <a href="#"><h4>Lorem ipsum dolor sit non, magni quidem laborum.</h4></a>
                                        <hr>
                                        <div class="media location mb-2">
                                            <i class="fas fa-map-marker-alt"></i>
                                            <div class="media-body">
                                                Cras sit amet nibh lientum nunc ac nisi vulputate fringilla.
                                            </div>
                                        </div>
                                        <div class="row m-0 mb-3">
                                            <div class="col-md-12 p-0 mb-2">
                                                <i class="fas fa-calendar-alt mr-2"></i>
                                                <span>5 Sep, 2019</span>
                                            </div>
                                            <div class="col-md-12 p-0">
                                                <i class="far fa-clock mr-2"></i>
                                                <span> 4 pm-7:00 pm</span>
                                            </div>
                                        </div>
                                        <span>Categories: Community</span>
                                        <hr>
                                        <a href="#" class="btn btn-primary event-submit-btn bg-orange">Buy Tickets/Free Events</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-md-12">
                                <div class="advertisement-widget">
                                    <img class="img-fluid" src="./assets/images/dummy/9520837773522296559.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-5">
                                <div class="event-box">
                                    <a href="#" class="d-block">
                                        <img src="./assets/images/event-item-3.png" alt="" class="img-fluid">
                                    </a>
                                    <div class="box-body">
                                        <a href="#"><h4>Lorem ipsum dolor sit non, magni quidem laborum.</h4></a>
                                        <hr>
                                        <div class="media location mb-2">
                                            <i class="fas fa-map-marker-alt"></i>
                                            <div class="media-body">
                                                Cras sit amet nibh lientum nunc ac nisi vulputate fringilla.
                                            </div>
                                        </div>
                                        <div class="row m-0 mb-3">
                                            <div class="col-md-12 p-0 mb-2">
                                                <i class="fas fa-calendar-alt mr-2"></i>
                                                <span>5 Sep, 2019</span>
                                            </div>
                                            <div class="col-md-12 p-0">
                                                <i class="far fa-clock mr-2"></i>
                                                <span> 4 pm-7:00 pm</span>
                                            </div>
                                        </div>
                                        <span>Categories: Community</span>
                                        <hr>
                                        <a href="#" class="btn btn-primary event-submit-btn bg-orange">Buy Tickets/Free Events</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-5">
                                <div class="event-box">
                                    <a href="#" class="d-block">
                                        <img src="./assets/images/event-item-2.png" alt="" class="img-fluid">
                                    </a>
                                    <div class="box-body">
                                        <a href="#"><h4>Lorem ipsum dolor sit non, magni quidem laborum.</h4></a>
                                        <hr>
                                        <div class="media location mb-2">
                                            <i class="fas fa-map-marker-alt"></i>
                                            <div class="media-body">
                                                Cras sit amet nibh lientum nunc ac nisi vulputate fringilla.
                                            </div>
                                        </div>
                                        <div class="row m-0 mb-3">
                                            <div class="col-md-12 p-0 mb-2">
                                                <i class="fas fa-calendar-alt mr-2"></i>
                                                <span>5 Sep, 2019</span>
                                            </div>
                                            <div class="col-md-12 p-0">
                                                <i class="far fa-clock mr-2"></i>
                                                <span> 4 pm-7:00 pm</span>
                                            </div>
                                        </div>
                                        <span>Categories: Community</span>
                                        <hr>
                                        <a href="#" class="btn btn-primary event-submit-btn bg-orange">Buy Tickets/Free Events</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="advertisement-widget">
                                    <img class="img-fluid" src="./assets/images/dummy/6318773323692451873.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <ul class="pagination-list">
                                <li><a href="#"><< Previous</a></li>
                                <li><a href="#">1</a></li>
                                <li class="active"><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">Next >></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <aside class="side-area mt-md-5 pt-md-4">

                            <div class="advertisement-widget">
                                <img class="img-fluid" src="./assets/images/dummy/495712436105197116.jpg" alt="">
                            </div>
                            <div class="widget calendar-widget">
                                <div id="eventsDatePicker"></div>
                            </div>
                            <div class="advertisement-widget">
                                <img class="img-fluid" src="./assets/images/dummy/1841746877016134826.jpg" alt="">
                            </div>
                            <div class="widget search-filter-widget">
                                <form>
                                    <div class="accordion" id="eventsFilter">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h2>
                                                    <button class="btn btn-link collapsed text-uppercase" type="button" data-toggle="collapse" data-target="#collapseEvents" aria-expanded="true" aria-controls="collapseOne">
                                                        Events Filter <i class="fa fa-angle-down mt-2 float-right"></i>
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseEvents" class="collapse" aria-labelledby="headingOne" data-parent="#eventsFilter">
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label>Date</label>
                                                        <input id="from_date" type="text" class="form-control" placeholder="From">
                                                    </div>
                                                    <div class="form-group">
                                                        <input id="to_date" type="text" class="form-control" placeholder="To">
                                                    </div>
                                                    <div class="filter-checkboxes">
                                                        <label class="text-uppercase">Categories</label>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input " id="customControlAutosizing1">
                                                            <label class="custom-control-label entertainment" for="customControlAutosizing1">Entertainment</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input " id="customControlAutosizing2">
                                                            <label class="custom-control-label artsCulture" for="customControlAutosizing2">Arts & Culture</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing3">
                                                            <label class="custom-control-label sports" for="customControlAutosizing3">Sports</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing4">
                                                            <label class="custom-control-label foodDining" for="customControlAutosizing4">Food & Dining</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing5">
                                                            <label class="custom-control-label other" for="customControlAutosizing5">Other</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing6">
                                                            <label class="custom-control-label community" for="customControlAutosizing6">Community </label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing7">
                                                            <label class="custom-control-label night" for="customControlAutosizing7">Night</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing8">
                                                            <label class="custom-control-label socialResoponsibility" for="customControlAutosizing8">SocialResoponsibility</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing9">
                                                            <label class="custom-control-label education" for="customControlAutosizing9">Education</label>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-purple btn-block mt-4">Yalla!</button>
                                                    <button type="button" class="btn btn-pink btn-block">Reset Filter</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="advertisement-widget sticky-widget">
                                <img class="img-fluid" src="./assets/images/dummy/1013443136016165343.png" alt="">
                            </div>
                        </aside>
                    </div>
                </div>
            </section>
        </div>
    </main>

<?php include_once "footer.php" ?>