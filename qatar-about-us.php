<?php include_once "header.php" ?>

    <section class="hero-section sub-hero">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-md-7">
                    <h1>just like you...</h1>
                    <p>we were tired of always hearing about <br>events after they were done.</p>
                </div>
                <div class="col-md-4">
                    <div class="g-logo-img">
                        <img class="g-logo" src="./assets/images/eq.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <main class="bg-darkgray">
        <div class="container bg-light">
            <section class="section about-us-content">
                <div class="row  my-5">
                    <div class="col-md-9">
                        <?php require "./inc/advertisement.php" ?>
                    </div>
                </div>

                <p class="section-desc">Waking up in the morning and hearing that everybody just went to a lit AF concert that you never heard is always a strange feeling. Did you wake up in another dimension?</p>
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <p class="mt-2">Well fret no more! The Qatar Events team has you covered. We started off as a twitter account that would share whatever events we could find online for you all! We then launched our
                            Facebook page and loads of you appreciated what we were doing.</p>
                        <img class="img-fluid" src="./assets/images/line-img.png" alt="">
                    </div>
                    <div class="col-md-2 text-right">
                        <img class="brace-img" src="./assets/images/brace-black.png" alt="">
                    </div>
                    <div class="col-md-4">
                        <div class="right text-center">
                            <h1>250,000</h1>
                            <h5>of you on</h5>
                            <ul class="social-links-lg mt-4">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <h1>We've Grown! <span>Grown A LOT.</span></h1>
                    <p>Not only are we a part of the ILQ Network, we've got our own weekend roundup show on youtube, and a weekly newsletter too! To take things to the next level, we've launched this app to make your lives
                        even more convenient. It might have a few bugs here and there, but we're committed to making this app rock and to regularly update it, so make sure you share your feedback with us. Make sure you also
                        follow some channels, you'll get notifications whenever they have a cool new event too! </p>
                </div>
            </section>
        </div>
    </main>

<?php include_once "footer.php" ?>