<?php include_once "header.php"?>
    <div class="sub-menu bg-skyblue">
        <div class="container">
            <nav class="sub-menu-nav capitalize text-center">
                <ul>
                    <li><a href="#">Categories: extraad</a></li>
                    <li><a href="#">Entertainment</a></li>
                    <li><a href="#">Sports</a></li>
                    <li><a href="#">Food & Dining</a></li>
                    <li><a class="active" href="#">Arts & Culture</a></li>
                    <li><a href="#">Night</a></li>
                    <li><a href="#">Community</a></li>
                    <li><a href="#">Social Responsibility</a></li>
                    <li><a href="#">Education</a></li>
                    <li><a href="#">Other</a></li>
                    <li><a href="#">View All</a></li>
                </ul>
                <ul>
                    <li><a href="#">Categories: extraad</a></li>
                    <li><a href="#">Entertainment</a></li>
                    <li><a href="#">Sports</a></li>
                    <li><a href="#">Food & Dining</a></li>
                    <li><a class="active" href="#">Arts & Culture</a></li>
                    <li><a href="#">Night</a></li>
                    <li><a href="#">Community</a></li>
                    <li><a href="#">Social Responsibility</a></li>
                    <li><a href="#">Education</a></li>
                    <li><a href="#">Other</a></li>
                    <li><a href="#">View All</a></li>
                </ul>
            </nav>
        </div>
    </div>
<main class="main bg-darkgray">
    <div class="container bg-light">
        <section class="section event-details-section">
            <div class="row">
                <div class="col-md-9">
                    <?php require "./inc/advertisement.php" ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <h1>Lorem ipsum dolor, sit amet consectetur adipisicing elit Quos corrupti culpa molestiae!</h1>
                    <div class="event-info">
                        <div class="event-status">
                            <div class="left">
                                <span class="m-0">Posted On: 28 August 2019 08:00 am</span>
                                <span>Updated On: 29 August 2019 02:32 pm</span>
                            </div>
                            <div class="right">
                                <span><strong>Categories :</strong> Community</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="user-info text-center">
                                    <div class="user-img"><img src="./assets/images/user-img.png" alt=""></div>
                                    <h4>john smith</h4>
                                    <p class="mb-2">Share on...</p>
                                    <ul class="social-links">
                                        <li class="face-book"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li class="twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li class="linked-in"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                    <p class="mb-2">125 shares</p>
                                    <button class="btn btn-pink-outline btn-rounded">Start Discussion</button>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <img src="./assets/images/hero.png" class="img-fluid" alt="">

                                <div class="event-details">
                                    <h2>Events Details</h2>
                                    <div class="google-map">
                                        <img class="img-fluid w-100" src="./assets/images/gmap.png" alt="">
                                    </div>
                                    <ul class="event-address my-4">
                                        <li><b>location:</b> The Qater National Library , Qatar</li>
                                        <li><b>Date:</b> 30 September 2019</li>
                                        <li><b>Time:</b> 10:30 am-12:30 pm</li>
                                        <li><b>Ticket Purchase:</b> Online</li>
                                        <li><b>Price:</b> General Entry -its-free</li>
                                        <li><b>Website:</b> <a href="#">https://www.qatarevents.com</a></li>
                                    </ul>
                                    <a href="#" class="btn btn-purple btn-rounded mb-5">Buy Ticket <i class="fas fa-check ml-2"></i></a>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa cumque enim quis natus. Eos distinctio veritatis reprehenderit facilis recusandae, cupiditate esse sed! Voluptatem
                                        nemo ea harum debitis corrupti, ipsum itaque!</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa cumque enim quis natus. Eos distinctio veritatis reprehenderit facilis recusandae, cupiditate esse sed! Voluptatem
                                        nemo ea harum debitis corrupti, ipsum itaque!</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa cumque enim quis natus. Eos distinctio veritatis reprehenderit facilis recusandae, cupiditate esse sed! Voluptatem
                                        nemo ea harum debitis corrupti, ipsum itaque!</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa cumque enim quis natus. Eos distinctio veritatis reprehenderit facilis recusandae, cupiditate esse sed! Voluptatem
                                        nemo ea harum debitis corrupti, ipsum itaque!</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa cumque enim quis natus. Eos distinctio veritatis reprehenderit facilis recusandae, cupiditate esse sed! Voluptatem
                                        nemo ea harum debitis corrupti, ipsum itaque!</p>
                                </div>
                                <div class="event-share-links">
                                    <h4>Liked this event? Share It!</h4>
                                    <ul class="social-links ml-2">
                                        <li class="face-book"><a href="#"><i class="fab fa-facebook-f"></a></i></li>
                                        <li class="twitter"><a href=#><i class="fab fa-twitter"></i></a></li>
                                        <li class="linked-in"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                        <li class="pinterest"><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                        <li class="email"><a href="#"><i class="far fa-envelope"></i></a></li>
                                    </ul>
                                </div>
                                <div class="other-events">
                                    <h4 class="heading-title">You Might Like It</h4>
                                    <div class="event-item bg-white mb-4">
                                        <div class="media mb-3">
                                            <img class="mr-3 img-fluid" src="./assets/images/image-3.png" alt="Generic placeholder image">
                                            <div class="media-body my-auto">
                                                <h4>Lorem ipsum dolor sit amet, coelit.amet consectetur , a corrupti uga velit?</h4>
                                                <ul class="information">
                                                    <li class="d-flex align-items-baseline mb-3"><i class="fas fa-map-marker-alt"></i><p class="m-0">Lorem ipsum dolor sit amet consectetur a .</p></li>
                                                    <li class="w-50 d-inline mr-2"><i class="fas fa-calendar-alt mr-2"></i><strong>05 Sep, 2019</strong></li>
                                                    <li class="w-50 d-inline"><i class="far fa-clock mr-2"></i><strong>04:00 pm-07:00pm</strong></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="d-flex left-widget">
                                                    <div class="user-info">
                                                        <div class="media">
                                                            <img class="mr-2" src="./assets/images/image-2.png" alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <h6>John Smith</h6>
                                                                <span>28 August 2019 07:00 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="categories">
                                                        <p><strong>Categories:</strong> <br><span>Community</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 text-center my-auto">
                                                <a href="#">
                                                    <button class="btn btn-pink-outline btn-rounded">Get Details</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="event-item bg-white mb-4">
                                        <div class="media mb-3">
                                            <img class="mr-3 img-fluid" src="./assets/images/image-3.png" alt="Generic placeholder image">
                                            <div class="media-body my-auto">
                                                <h4>Lorem ipsum dolor sit amet, coelit.amet consectetur , a corrupti uga velit?</h4>
                                                <ul class="information">
                                                    <li class="d-flex align-items-baseline mb-3"><i class="fas fa-map-marker-alt"></i><p class="m-0">Lorem ipsum dolor sit amet consectetur a .</p></li>
                                                    <li class="w-50 d-inline mr-2"><i class="fas fa-calendar-alt mr-2"></i><strong>05 Sep, 2019</strong></li>
                                                    <li class="w-50 d-inline"><i class="far fa-clock mr-2"></i><strong>04:00 pm-07:00pm</strong></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="d-flex left-widget">
                                                    <div class="user-info">
                                                        <div class="media">
                                                            <img class="mr-2" src="./assets/images/image-2.png" alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <h6>John Smith</h6>
                                                                <span>28 August 2019 07:00 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="categories">
                                                        <p><strong>Categories:</strong> <br><span>Community</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 text-center my-auto">
                                                <a href="#">
                                                    <button class="btn btn-pink-outline btn-rounded">Get Details</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="event-item bg-white mb-4">
                                        <div class="media mb-3">
                                            <img class="mr-3 img-fluid" src="./assets/images/image-4.png" alt="Generic placeholder image">
                                            <div class="media-body my-auto">
                                                <h4>Lorem ipsum dolor sit amet, coelit.amet consectetur , a corrupti uga velit?</h4>
                                                <ul class="information">
                                                    <li class="d-flex align-items-baseline mb-3"><i class="fas fa-map-marker-alt"></i><p class="m-0">Lorem ipsum dolor sit amet consectetur a .</p>.</li>
                                                    <li class="w-50 d-inline mr-2"><i class="fas fa-calendar-alt mr-2"></i><strong>05 Sep, 2019</strong></li>
                                                    <li class="w-50 d-inline"><i class="far fa-clock mr-2"></i><strong>04:00 pm-07:00pm</strong></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="d-flex left-widget">
                                                    <div class="user-info">
                                                        <div class="media">
                                                            <img class="mr-2" src="./assets/images/image-2.png" alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <h6>John Smith</h6>
                                                                <span>28 August 2019 07:00 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="categories">
                                                        <p><strong>Categories:</strong> <br><span>Community</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 text-center my-auto">
                                                <a href="#">
                                                    <button class="btn btn-pink-outline btn-rounded">Get Details</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="event-item bg-white mb-4">
                                        <div class="media mb-3">
                                            <img class="mr-3 img-fluid" src="./assets/images/image-5.png" alt="Generic placeholder image">
                                            <div class="media-body my-auto">
                                                <h4>Lorem ipsum dolor sit amet, coelit.amet consectetur , a corrupti uga velit?</h4>
                                                <ul class="information">
                                                    <li class="d-flex align-items-baseline mb-3"><i class="fas fa-map-marker-alt"></i><p class="m-0">Lorem ipsum dolor sit amet consectetur a.</p></li>
                                                    <li class="w-50 d-inline mr-2"><i class="fas fa-calendar-alt mr-2"></i><strong>05 Sep, 2019</strong></li>
                                                    <li class="w-50 d-inline"><i class="far fa-clock mr-2"></i><strong>04:00 pm-07:00pm</strong></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="d-flex left-widget">
                                                    <div class="user-info">
                                                        <div class="media">
                                                            <img class="mr-2" src="./assets/images/image-2.png" alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <h6>John Smith</h6>
                                                                <span>28 August 2019 07:00 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="categories">
                                                        <p><strong>Categories:</strong> <br><span>Community</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 text-center my-auto">
                                                <a href="#">
                                                    <button class="btn btn-pink-outline btn-rounded">Get Details</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3">
                    <aside class="side-area">
                        <a href="#" class="btn btn-pink btn-lg btn-block btn-rounded btn-icon-right mb-4">Submit Events <i class="fa fa-arrow-right"></i></a>
                        <div class="widget categories-links">
                            <h1>Categories :</h1>
                            <ul>
                                <li><a class="bg-pink" href="#">Entertainment</a></li>
                                <li><a class="bg-pink" href="#">Sports</a></li>
                                <li><a class="bg-pink" href="#">Night</a></li>
                                <li><a class="bg-pink" href="#">Food &amp; dining</a></li>
                                <li><a class="bg-pink" href="#">Social Responsibility</a></li>
                                <li><a class="bg-pink" href="#">Art &amp; Culture</a></li>
                                <li><a class="bg-pink" href="#">Community</a></li>
                                <li><a class="bg-pink" href="#">Education</a></li>
                                <li><a class="bg-pink" href="#">Other</a></li>
                            </ul>
                        </div>
                        <div class="advertisement-widget">
                            <img class="img-fluid" src="./assets/images/dummy/495712436105197116.jpg" alt="">
                        </div>
                        <div class="widget tweets">
                            <h6>Tweets</h6>
                            <img class="img-fluid" src="./assets/images/tweet.png" alt="">
                        </div>
                        <div class="advertisement-widget">
                            <img class="img-fluid" src="./assets/images/dummy/1841746877016134826.jpg" alt="">
                        </div>
                        <div class="widget news-widget">
                            <div class="widget-tabs">
                                <ul class="nav nav-tabs d-flex justify-content-start pb-2">
                                    <li>
                                        <a href="#tab_news" data-toggle="tab">News</a>
                                    </li>
                                    <li>
                                        <a class="active" href="#tab_videos" data-toggle="tab">Videos</a>
                                    </li>
                                    <li>
                                        <a href="#tab_event" data-toggle="tab">Events</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane" id="tab_news">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Est aperiam iusto ex reprehenderit necessitatibus nobis eos sint tempore sed qui, dicta et ea nihil
                                            sapiente? Quibusdam expedita facere consequuntur saepe?</p>
                                    </div>
                                    <div class="tab-pane active" id="tab_videos">
                                        <div class="embed-responsive embed-responsive-16by9 tab-video">
                                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/v64KOxKVLVg" allowfullscreen=""></iframe>
                                        </div>
                                        <p class="mt-2">Lorem ipsum dolor sitlit aperiam, commodi id voluptatem!</p>
                                    </div>
                                    <div class="tab-pane" id="tab_event">
                                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iusto pariatur suscipit aliquam veritatis. Exercitationem minima reiciendis aut nisi, amet qui
                                            accusamus atque. Dicta eius quidem maiores atque eveniet sequi magnam.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advertisement-widget sticky-widget">
                            <img class="img-fluid" src="./assets/images/dummy/1013443136016165343.png" alt="">
                        </div>
                    </aside>
                </div>
            </div>
        </section>
        <section class="response-section">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h4>What do you think?</h4>
                    <p>0 Responses</p>
                    <ul class="response-btns">
                        <li><a href="#"><img src="./assets/images/upvote.png" alt=""> Upvote</a></li>
                        <li><a href="#"><img src="./assets/images/funny.png" alt=""> Funny</a></li>
                        <li><a href="#"><img src="./assets/images/love-icon.png" alt=""> Love</a></li>
                        <li><a href="#"><img src="./assets/images/surprised-icon.png" alt=""> Surprised</a></li>
                        <li><a href="#"><img src="./assets/images/angry-icon.png" alt=""> Angry</a></li>
                        <li><a href="#"><img src="./assets/images/sad.png" alt=""> Sad</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="section-comment-tabs">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="comment-tabs">
                        <div class="login-btn-area">
                            <p class="notifications m-0">1</p>
                            <div class="dropdown">
                                <button class="btn sort-btn dropdown-toggle" type="button" id="login-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Login
                                </button>
                                <div class="dropdown-menu" aria-labelledby="login-button">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-pills mb-3">
                            <li class="active">
                                <a class="active" href="#commentsTab" data-toggle="tab">0 Comments</a>
                            </li>
                            <li>
                                <a href="#iLoveQatarTab" data-toggle="tab">iLoveQatar.net</a>
                            </li>
                        </ul>
                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="commentsTab">
                                <div class="row mb-4">
                                    <div class="col-md-6">
                                        <ul class="action-feedback">
                                            <li class="recommend-link"><a href="#"><i class="far fa-heart"></i>Recommend</a></li>
                                            <li class="twitter-link"><a href="#"><i class="fab fa-twitter"></i>Tweet</a></li>
                                            <li class="face-book-link"><a href="#"><i class="fab fa-facebook-f"></i>Share</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="sort-list-dd">
                                            <div class="dropdown">
                                                <button class="btn sort-btn dropdown-toggle" type="button" id="sort-list-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Sort By best
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="sort-list-button">
                                                    <a class="dropdown-item" href="#">Action</a>
                                                    <a class="dropdown-item" href="#">Another action</a>
                                                    <a class="dropdown-item" href="#">Something else here</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center user-discution-field mb-3">
                                    <div class="col-md-1 pr-0">
                                        <div class="user-image">
                                            <img src="./assets/images/user-icon.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-11">
                                        <form action="">
                                            <input class="form-control" type="text" placeholder="Start the Discussion...">
                                        </form>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-11 offset-md-1 pl-0">
                                        <div class="login-signup">
                                            <div class="row ml-0">
                                                <div class="col-md-5 col-lg-3">
                                                    <div class="login">
                                                        <h6>Log in with</h6>
                                                        <ul class="links">
                                                            <li class="d-link first"><a href="#"><i class="fab fa-dochub"></i></a></li>
                                                            <li class="face-book"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                            <li class="d-link"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                            <li class="google"><a href="#"><i class="fab fa-google"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-7 col-lg-9 pl-0">
                                                    <div class="sing-up">
                                                        <h6>Or Sing up with Disqus <i class="far fa-question-circle"></i></h6>
                                                        <form action="">
                                                            <input class="form-control" type="text" placeholder="Name">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="iLoveQatarTab">
                                <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias facilis, assumenda itaque atque, perspiciatis voluptatum distinctio quibusdam fugit
                                    nulla temporibus voluptates dolores perferendis necessitatibus a quae repellendus magni porro corporis. enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>

<?php include_once "footer.php"?>